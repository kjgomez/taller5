#include <stdio.h>
#include "cabecera.h"

void bytesInt(int valor){
	int n=sizeof(valor);
	for (int i=0; i<n; i++) {
		unsigned char *v=&valor;
		unsigned char byte=*(v+i);
		printf("Byte %d: %02X\n",i,byte);
	}
}

void bytesLong(long valor){
	int n=sizeof(valor);
	for (int i=0; i<n; i++) {
		unsigned char *v=&valor;
		unsigned char byte=*(v+i);
		printf("Byte %d: %02X\n",i,byte);
	}	
}

void bytesFloat(float valor){
	int n=sizeof(valor);
	for (int i=0; i<n; i++) {
		unsigned char *v=&valor;
		unsigned char byte=*(v+i);
		printf("Byte %d: %02X\n",i,byte);
	}
}

void bytesDouble(double valor){
	int n=sizeof(valor);
	for (int i=0; i<n; i++) {
		unsigned char *v=&valor;
		unsigned char byte=*(v+i);
		printf("Byte %d: %02X\n",i,byte);
	}
}
